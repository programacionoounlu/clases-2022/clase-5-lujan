package ar.edu.unlu.prendas.productos;

public class Prenda {
	private String codigo;
	private String nombre;
	private double precioLista;
	
	public Prenda(String codigo, String nombre) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPrecioLista() {
		return precioLista;
	}

	public void setPrecioLista(double precioLista) {
		this.precioLista = precioLista;
	}

	public String getCodigo() {
		return codigo;
	}
	
	public double calcularPrecioVenta() {
		return this.precioLista * 1.1;
	}
	
}
