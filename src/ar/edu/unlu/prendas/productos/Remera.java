package ar.edu.unlu.prendas.productos;

public class Remera extends Prenda {

	private double impuesto;
	
	public Remera(String codigo, String nombre, double impuesto) {
		super(codigo, nombre);
		this.setImpuesto(impuesto);
		
	}

	public double getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(double impuesto) {
		this.impuesto = impuesto;
	}

	public double calcularPrecioVenta() {
		return super.calcularPrecioVenta() + impuesto;
	}

	
}
