package ar.edu.unlu.prendas.productos;

public class CamisaML extends Prenda {
	
	public CamisaML(String codigo,String nombre) {
		super(codigo,nombre);
	}
	
	public double calcularPrecioVenta() {
		return super.calcularPrecioVenta() * 1.05;
	}

}
