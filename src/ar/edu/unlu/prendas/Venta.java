package ar.edu.unlu.prendas;

import java.util.ArrayList;

import ar.edu.unlu.prendas.pagos.MedioDePago;
import ar.edu.unlu.prendas.productos.Prenda;

public class Venta {
	private ArrayList<Prenda> prendas = new ArrayList<Prenda>();
	private MedioDePago medioPago;
	
	
	public void addPrenda(Prenda prenda) {
		prendas.add(prenda);
	}


	public MedioDePago getMedioPago() {
		return medioPago;
	}


	public void setMedioPago(MedioDePago medioPago) {
		this.medioPago = medioPago;
	}

	public double getSubTotal() {
		double total = 0;
		
		for(Prenda p : prendas) 
			total += p.calcularPrecioVenta();
		
		return total;
	}
	
	public double getTotal() {
		return medioPago.calcularPrecioFinal(getSubTotal());
	}
	
	
	
}
