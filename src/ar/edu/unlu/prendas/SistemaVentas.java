package ar.edu.unlu.prendas;

import ar.edu.unlu.prendas.pagos.MedioDePago;
import ar.edu.unlu.prendas.pagos.Tarjeta;
import ar.edu.unlu.prendas.pagos.TarjetaDorada;
import ar.edu.unlu.prendas.productos.CamisaML;
import ar.edu.unlu.prendas.productos.Prenda;
import ar.edu.unlu.prendas.productos.Remera;

public class SistemaVentas {
	
	public static void main(String[] args) {
		Venta v = new Venta();
		v.setMedioPago(new TarjetaDorada());
		
		Prenda p = new Prenda("001","Pantalon");
		p.setPrecioLista(100.0);
		v.addPrenda(p);
		
		
		Prenda p2 = new CamisaML("0CML1","Camisa Manga Larga Roja");
		p2.setPrecioLista(100.0);
		v.addPrenda(p2);
		


		Prenda r = new Remera("0R1","Camisa Manga Larga Roja",100.0);
		r.setPrecioLista(200.0);
		((Remera) r).setImpuesto(150);
		v.addPrenda(r);
		
		
		System.out.printf("El subtotal es: $ %.2f \n ",v.getSubTotal());
		System.out.printf("El total es: $ %.2f \n ",v.getTotal());

		
		
	}

}
