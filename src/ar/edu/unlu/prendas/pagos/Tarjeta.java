package ar.edu.unlu.prendas.pagos;

public class Tarjeta extends MedioDePago {

	public double calcularPrecioFinal(double precio) {
		double promo = precio * 0.01;
		return (precio - promo);
	}

}
